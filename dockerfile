FROM python:3.4

RUN mkdir -p /usr/src/StudentInfo
WORKDIR /usr/src/StudentInfo
COPY . /usr/src/StudentInfo
RUN pip install -r test-requirements.txt
CMD [ "python3", "./manage.py", "runserver", "0.0.0.0:8000"]





